import os
import csv
import requests
import xml.etree.ElementTree as ET
import mysql.connector
import json
import logging
import re
import time

path="/var/www/kssir/python"
os.chdir(path)

timestr = time.strftime("%Y_%m_%d__%H_%M_%S")

log_file = "/var/www/kssir/python/logs/"+timestr+".log"
logging.basicConfig(filename=log_file,level=logging.DEBUG)

logging.info('Initializing XML Data Import Process...')

print("Script started")
CONFIG = {}
CONFIG['db_config'] = {
    'db': 'dev',
    'user': 'dev',
    'password': 'p@as!23',
    'host': 'localhost'
}
CONFIG['xml_src_file'] = 'source.xml'

CONFIG['table'] = {
    'properties': {
        'pk': 'reference',
        'name': 'properties'
    },
    'property_types': {
        'pk': 'id',
        'name': 'property_types'
    },
    'property_categories': {
        'pk': 'id',
        'name': 'property_categories'
    },
    'countries': {
        'pk': 'id',
        'name': 'countries'
    },
    'provinces': {
        'pk': 'id',
        'name': 'provinces'
    },
    'cities': {
        'pk': 'id',
        'name': 'cities'
    },
    'subareas': {
        'pk': 'id',
        'name': 'subareas'
    },
    'garages': {
        'pk': 'id',
        'name': 'garages'
    },
    'gardens': {
        'pk': 'id',
        'name': 'gardens'
    },
    'pools': {
        'pk': 'id',
        'name': 'pools'
    },
    'options': {
        'pk': 'id',
        'name': 'options'
    }
}

def loadXML():
    print("Reading data from xml")
    # url of xml data feed
    url = 'https://xml.inmobalia.com/504/cb567ea86ffcc8833350e44fbec4717a.xml'
    logging.info("Reading data from url"+ url)
 
    # creating HTTP response object from given url
    resp = requests.get(url)
 
    print("Writing xml to file")
    # saving the xml file
    with open(CONFIG['xml_src_file'], 'wb') as f:
        f.write(resp.content)
    logging.info("Xml file updated with data.")
    return True
         
 
def parseXML(xmlfile):
    print("parsing XML data")
    # create element tree object
    logging.info("Parsing XML data...")
    tree = ET.parse(xmlfile)
    root = tree.getroot()
    
    # get root element
    # create empty list for properties
    properties = []
 
    # iterate property items
    for item in root.findall('property'):
 
        # empty gallery dictionary
        id = None
        keys = []
        vals = []
        gallery = []
        options_set = []
        description_set = []

        # iterate child elements of item
        for child in item:
            keys.append(child.tag)
            if child.tag == "reference":
                id = child.text
            if child.tag == 'images':
                for image in child:
                    gallery.append({
                        'url': image.attrib['url'],
                        'title': image.text
                    })
            if child.tag == 'options':
                for option in child:
                    options_set.append({
                        'type': option.attrib['type'],
                        'title': option.text
                    })
                    # Table 10 options
                    data_id = option.attrib['type']
                    child_text = option.text.encode('utf8')
                    if option.attrib['type']:
                        if not check_if_record_exists(data_id, CONFIG['table']['options']['pk'], CONFIG['table']['options']['name']):
                            save_or_insert(["id", "name"], [data_id,child_text], CONFIG['table']['options']['name'])

            if child.tag == 'descriptions':
                short_description_set = []
                long_description_set = []
                for desc in child:
                    lang_text = ''
                    if desc.attrib['lang']:
                        lang_text = desc.attrib['lang']
                    desc_text = ''
                    if desc.text:
                        desc_text = desc.text
                    if desc.tag == 'short_description':
                        short_description_set.append({
                            'lang': lang_text,
                            'text': desc_text
                        })
                    if desc.tag == 'long_description':
                        long_description_set.append({
                            'lang': lang_text,
                            'text': desc_text
                        })
                description_set.append({
                    'short_description': short_description_set,
                    'long_description': long_description_set
                })
            if child.tag in ['propertytype', 'propertycategory', 'country', 'province', 'cityname', 'subarea', 'tr_garage', 'tr_garden', 'tr_pool']:
                if not child.text:
                    child_text = ""
                else:
                    child_text = child.text

                if 'id' in child.attrib.keys():
                    data_id = child.attrib['id']
                else:
                    data_id = ''
                # Saving data to meta tables

                # Table 1 property_types
                if child.tag == 'propertytype' and data_id != "":
                    if not check_if_record_exists(data_id, CONFIG['table']['property_types']['pk'], CONFIG['table']['property_types']['name']):
                        save_or_insert(["id", "name", "slug"], [data_id,child_text, child_text.replace(" ", "-").lower()], CONFIG['table']['property_types']['name'])

                # Table 2 property_categories
                if child.tag == 'propertycategory' and data_id != "":
                    if not check_if_record_exists(data_id, CONFIG['table']['property_categories']['pk'], CONFIG['table']['property_categories']['name']):
                        save_or_insert(["id", "name", "slug"], [data_id,child_text,child_text.replace(" ", "-").lower()], CONFIG['table']['property_categories']['name'])

                # Table 3 countries
                if child.tag == 'country' and data_id != "":
                    if not check_if_record_exists(data_id, CONFIG['table']['countries']['pk'], CONFIG['table']['countries']['name']):
                        save_or_insert(["id", "name", "slug"], [data_id,child_text,child_text.replace(" ", "-").lower()], CONFIG['table']['countries']['name'])

                # Table 4 province
                if child.tag == 'province' and data_id != "":
                    if not check_if_record_exists(data_id, CONFIG['table']['provinces']['pk'], CONFIG['table']['provinces']['name']):
                        save_or_insert(["id", "name", "slug"], [data_id,child_text,child_text.replace(" ", "-").lower()], CONFIG['table']['provinces']['name'])

                # Table 5 cities
                if child.tag == 'cityname' and data_id != "":
                    if not check_if_record_exists(data_id, CONFIG['table']['cities']['pk'], CONFIG['table']['cities']['name']):
                        save_or_insert(["id", "name", "slug"], [data_id,child_text, child_text.replace(" ", "-").lower()], CONFIG['table']['cities']['name'])

                # Table 6 subareas
                if child.tag == 'subarea' and data_id != "":
                    if not check_if_record_exists(data_id, CONFIG['table']['subareas']['pk'], CONFIG['table']['subareas']['name']):
                        save_or_insert(["id", "name", "slug"], [data_id,child_text,child_text.replace(" ", "-").lower()], CONFIG['table']['subareas']['name'])
                
                # Table 7 garages
                if child.tag == 'tr_garage' and data_id != "":
                    if not check_if_record_exists(data_id, CONFIG['table']['garages']['pk'], CONFIG['table']['garages']['name']):
                        save_or_insert(["id", "name"], [data_id,child_text], CONFIG['table']['garages']['name'])

                # Table 8 gardens
                if child.tag == 'tr_garden' and data_id != "":
                    if not check_if_record_exists(data_id, CONFIG['table']['gardens']['pk'], CONFIG['table']['gardens']['name']):
                        save_or_insert(["id", "name"], [data_id,child_text], CONFIG['table']['gardens']['name'])

                # Table 9 pools
                if child.tag == 'tr_pool' and data_id != "":
                    if not check_if_record_exists(data_id, CONFIG['table']['pools']['pk'], CONFIG['table']['pools']['name']):
                        save_or_insert(["id", "name"], [data_id,child_text], CONFIG['table']['pools']['name'])

                vals.append(data_id)

            elif child.tag == 'images':
                vals.append(re.escape(json.dumps(gallery, ensure_ascii=False)))
            elif child.tag == 'options':
                vals.append(re.escape(json.dumps(options_set, ensure_ascii=False)))
            elif child.tag == 'descriptions':
                vals.append(re.escape(json.dumps(description_set, ensure_ascii=False)))
            else:
                if not child.text:
                    child_text = ""
                else:
                    child_text = child.text
                vals.append(child_text)
            if child.tag == "reference":
                ## Add slug to properties
                keys.append("slug")
                vals.append("property_"+child_text.replace(" ", "-").lower())

        #  Before inserting data into table
        # Perform necesary inserts into related tables

        if check_if_record_exists(id, CONFIG['table']['properties']['pk'], CONFIG['table']['properties']['name']):
            # updating record
            print("Record exists, updating record")
            update(keys, vals, id, CONFIG['table']['properties']['pk'], CONFIG['table']['properties']['name'])
        else:
            # Inserting record
            print("Inserting new record")
            save_or_insert(keys, vals, CONFIG['table']['properties']['name'])
        ##print("End of parseXML")
    logging.info("Updated/modified %s records."%(len(properties)))
    return properties

def check_if_record_exists(id, pk, table_name):
    cnx = mysql.connector.connect(host=CONFIG['db_config']['host'],database=CONFIG['db_config']['db'],user=CONFIG['db_config']['user'],password=CONFIG['db_config']['password'])
    cursor = cnx.cursor()
    sql = ("SELECT * FROM %s "
               " WHERE %s = '%s'")%(table_name, pk, id)
    cursor.execute(sql)
    row = cursor.fetchone()
    ##print("Checking if record exists", id, pk, table_name, row)
    if row is not None:
        return True
    return False

def update(keys, vals, id, pk, table_name):
    logging.info("Preparing SQL execution...")
    cnx = mysql.connector.connect(host=CONFIG['db_config']['host'],database=CONFIG['db_config']['db'],user=CONFIG['db_config']['user'],password=CONFIG['db_config']['password'])
    cursor = cnx.cursor()
    keys_str = ""
    for i in range(0, len(keys)):
        keys_str += "{0}='{1}',".format(keys[i], vals[i])
    keys_str = keys_str.rstrip(",")
    # update_sql = ("UPDATE %s "
    #            " SET %s "
    #            "WHERE %s='%s'")%(table_name, keys_str, pk, id)
    update_sql = ("UPDATE {0} "
               " SET {1} "
               "WHERE {2}='{3}'").format(table_name, keys_str, pk, id)
    cursor.execute(update_sql)
    # print("Keys=", update_sql, "Vals=", vals_str)
    cnx.commit()
    cursor.close()
    cnx.close()
    logging.info("SQL executed:%s"%(update_sql))
    ##print("Records inserted successfully")
    

def save_or_insert(keys, vals, table_name):
    logging.info("Preparing SQL execution...")
    cnx = mysql.connector.connect(host=CONFIG['db_config']['host'],database=CONFIG['db_config']['db'],user=CONFIG['db_config']['user'],password=CONFIG['db_config']['password'])
    cursor = cnx.cursor()
    keys_str = ",".join(keys)
    vals_str = ",".join(vals)
    keys_commas = ""
    for k in keys:
        keys_commas += "%s,"
    keys_commas = keys_commas.rstrip(",")
    add_props_sql = ("INSERT INTO %s "
               "(%s) "
               "VALUES (%s)")%(table_name, keys_str, keys_commas)
    cursor.execute(add_props_sql, vals)
    # print("Keys=", add_props_sql, "Vals=", vals_str)
    ##print(add_props_sql)
    logging.info("SQL executed:%s"%(add_props_sql))
    cnx.commit()
    cursor.close()
    cnx.close()
    ##print("Records inserted successfully") 
     
def main():
    # load rss from web to update existing xml file
    loadXML()
 
    # parse xml file
    properties = parseXML('source.xml')
    
    print ("End of script")
    logging.info("End of script...")
    # store news items in a csv file
    # savetoCSV(properties, 'properties.csv')
     
     
if __name__ == "__main__":
 
    # calling main function
    main()

